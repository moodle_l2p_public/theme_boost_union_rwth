<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Union RWTH - Hook: Allows plugins to add any elements to the page <head> html tag.
 *
 * @package    theme_boost_union_rwth
 * @copyright  2024 Marvin Kirch <m.kirch@itc.rwth-aachen.de>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace theme_boost_union_rwth\hook\output;

/**
 * Hook to allow plugins to add any elements to the page <head> html tag.
 *
 * @package    theme_boost_union_rwth
 * @copyright  2024 Marvin Kirch <m.kirch@itc.rwth-aachen.de>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class before_footer_html_generation {
    /**
     * Callback to add head elements.
     *
     * @param \core\hook\output\before_footer_html_generation $hook
     */
    public static function callback(\core\hook\output\before_footer_html_generation $hook): void {
        global $PAGE, $CFG;
        $js = file_get_contents($CFG->dirroot.'/theme/boost_union_rwth/javascript/clamp.min.js');
        $js .= <<<EOS
            // forEach method, could be shipped as part of an Object Literal/Module
            var forEach = function (array, callback, scope) {
                for (var i = 0; i < array.length; i++) {
                callback.call(scope, i, array[i]); // passes back stuff we need
                }
            };
    
            var myNodeList = document.querySelectorAll('.line-clamp-2');
            forEach(myNodeList, function (index, value) {
                \$clamp(value, {clamp: 2});
            });
            var myNodeList = document.querySelectorAll('.line-clamp-3');
            forEach(myNodeList, function (index, value) {
                \$clamp(value, {clamp: 3});
            });
            var myNodeList = document.querySelectorAll('.line-clamp-4');
            forEach(myNodeList, function (index, value) {
                \$clamp(value, {clamp: 4});
            });
            var myNodeList = document.querySelectorAll('.line-clamp-5');
            forEach(myNodeList, function (index, value) {
                \$clamp(value, {clamp: 5});
            });
            EOS;
        $PAGE->requires->js_amd_inline($js);
        $PAGE->requires->js_call_amd('theme_boost_union_rwth/cookiebanner', 'init');    
    }
}
