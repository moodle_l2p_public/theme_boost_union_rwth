// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cookie Banner
 *
 * @module  theme/boost_union_rwth/cookiebanner
 * @author  2023 ITC RWTH Aachen
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

import * as Str from 'core/str';
import Config from 'core/config';

const getCookie = name => {
    let cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; ++i) {
        let cookie = cookies[i].trim();
        let cname, cvalue;
        [cname, cvalue] = cookie.split("=", 2);
        if (cname == name) {
            return cvalue;
        }
    }
    return false;
};

const setCookie = (name, value) => {
    let path = new URL(M.cfg.wwwroot).pathname;
    if (!path.endsWith("/")) {
        path += "/";
    }

    var d = new Date();
    d.setDate(d.getDate() + 400);

    document.cookie = name + "=" + value + "; expires=" + d.toUTCString() + ";path=" + path;
};

export const init = () => {
    "use strict";

    if (document.body.classList.contains('pagelayout-embedded')) {
        return;
    }

    let cvalue = getCookie("cookies-confirm");
    if (cvalue === "true") {
        // Extend cookie expiration.
        setCookie("cookies-confirm", "true");
    } else {
        // Create a cookie container.
        var cookieContainer = document.createElement('div');
        cookieContainer.setAttribute("id", "cookie-container");
        cookieContainer.style.zIndex = '1200';
        cookieContainer.classList.add("align-middle");

        // Create a cookie text.
        var cookieTextElement = document.createElement('p');
        cookieTextElement.setAttribute("id", "cookie-container-text");
        Str.get_string('cookie-banner-text', 'theme_boost_union_rwth')
            .done((confirmstr) => {
                confirmstr = confirmstr.replace(/&nbsp;/g, ' ');
                var cookieText = document.createTextNode(confirmstr);
                cookieTextElement.appendChild(cookieText);
            });
        cookieTextElement.style.marginBottom = '0px';
        cookieTextElement.style.marginTop = '0px';
        cookieContainer.appendChild(cookieTextElement);

        // Create further info anchor-tag.
        var furtherinfoElement = document.createElement('a');
        furtherinfoElement.setAttribute("id", "further-info");
        Str.get_string('cookie-furtherinfo-url', 'theme_boost_union_rwth')
            .done((confirmstr) => {
                furtherinfoElement.setAttribute("href", Config.wwwroot + confirmstr);
            });
        Str.get_string('cookie-furtherinfo', 'theme_boost_union_rwth')
            .done((confirmstr) => {
                var furtherinfoText = document.createTextNode(confirmstr);
                furtherinfoElement.appendChild(furtherinfoText);
            });
        furtherinfoElement.style.marginLeft = '4px';
        cookieContainer.appendChild(furtherinfoElement);

        // Create confirm "button".
        var cookieConfirmButtonElement = document.createElement('a');
        cookieConfirmButtonElement.setAttribute("id", "cookie-confirm-btn");
        cookieConfirmButtonElement.setAttribute("href", "#");
        Str.get_string('cookie-confirm-button', 'theme_boost_union_rwth')
            .done((confirmstr) => {
                var cookieConfirmButtonText = document.createTextNode(confirmstr);
                cookieConfirmButtonElement.appendChild(cookieConfirmButtonText);
            });
        cookieConfirmButtonElement.classList.add('btn', 'btn-primary');
        cookieConfirmButtonElement.style.marginLeft = '30px';
        cookieContainer.appendChild(cookieConfirmButtonElement);

        cookieContainer.style.right = '0px';
        cookieContainer.style.bottom = '0px';
        cookieContainer.style.backgroundColor = '#FFFFFF';
        cookieContainer.style.minHeight = '70px';
        cookieContainer.style.width = '100%';
        cookieContainer.style.position = 'fixed';
        cookieContainer.style.boxShadow = '0 0 6px -2px rgb(0 0 0 / 26%)';

        cookieConfirmButtonElement.position = 'fixed';

        furtherinfoElement.position = 'fixed';

        document.getElementsByTagName("body")[0].appendChild(cookieContainer);

        document.querySelector('#cookie-confirm-btn').addEventListener("click",
            function() {
                setCookie("cookies-confirm", "true");
                setTimeout(function() {
                    document.querySelector('#cookie-container').remove();
                }, 200);
            }
        );
    }
};
