<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Union RWTH - Version file
 *
 * @package    theme_boost_union_rwth
 * @author     2023 IT Center RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->component = 'theme_boost_union_rwth';
$plugin->version = 2025011000;
$plugin->release = 'v1.4.0';
$plugin->requires = 2023100900;
$plugin->supported = [405, 405];
$plugin->maturity = MATURITY_STABLE;
$plugin->dependencies = [
    'theme_boost_union' => 2024100703,
    'local_rwth_settings_adjustment' => 2023031500
];
