<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Union RWTH - Frontpage Layout file.
 *
 * @package   theme_boost_union_rwth
 * @copyright 2023 Josha Bartsch
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

//require_once($CFG->libdir . '/behat/lib.php');

$bodyattributes = $OUTPUT->body_attributes();

$languagedata = new \core\output\language_menu($PAGE);

$languagemenu = $languagedata->export_for_template($OUTPUT);

// Add the additional layouts to the template context.
$templatecontext = [
    'bodyattributes' => $bodyattributes,
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'langmenu' => $languagemenu,
];

// Include the template content for the footnote.
require_once($CFG->dirroot . '/theme/boost_union_rwth/layout/includes/footnote.php');

// Include the template content for the static pages.
require_once($CFG->dirroot . '/theme/boost_union/layout/includes/staticpages.php');

// Include the template content for the footer button.
require_once($CFG->dirroot . '/theme/boost_union/layout/includes/footer.php');

// Include the template content for the JavaScript disabled hint.
require_once($CFG->dirroot . '/theme/boost_union/layout/includes/javascriptdisabledhint.php');

// Include the template content for the info banners.
require_once($CFG->dirroot . '/theme/boost_union/layout/includes/infobanners.php');

// Include the template content for the navbar styling.
require_once($CFG->dirroot . '/theme/boost_union/layout/includes/navbar.php');

// Include the template content for the smart menus.
require_once($CFG->dirroot . '/theme/boost_union/layout/includes/smartmenus.php');

echo $OUTPUT->render_from_template('theme_boost_union_rwth/frontpage', $templatecontext);

