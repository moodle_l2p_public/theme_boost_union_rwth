<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Union RWTH
 *
 * @package   theme_boost_union_rwth
 * @copyright 2023 Hendrik Donath <Donath@itc-rwth.aachen.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/badgeslib.php');

global $PAGE, $USER;

if ($PAGE->user_is_editing()) {
    return;
}
$blockmanager = $PAGE->blocks;
$blockmanager->load_blocks(true);
foreach ($blockmanager->get_regions() as $region) {
    foreach ($blockmanager->get_blocks_for_region($region) as $block) {
        $instance = $block->instance;
        if ($instance->blockname != "badges") {
            continue;
        }
        $badges = badges_get_user_badges($USER->id);
        if (empty($badges)) {
            $instance->visible = "0";
        }
    }
}