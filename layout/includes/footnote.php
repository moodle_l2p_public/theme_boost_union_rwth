<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * COPIED FROM THEME/BOOST_UNION
 * 
 * Theme Boost Union RWTH - Footnote layout include.
 *
 * @package   theme_boost_union_rwth
 * @copyright 2024 Marvin Kirch, RWTH Aachen University m.kirch@itc.rwth-aachen.de
 * @copyright based on code from theme_boost_union by Luca Bösch
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$footnote = $OUTPUT->render_from_template('theme_boost_union/footnote', $templatecontext);

// Only proceed if text area does not only contains empty tags.
if (!html_is_blank($footnote)) {
    // Use format_text function to enable multilanguage filtering.
    //$footnote = format_text($footnote, FORMAT_HTML, ['noclean' => false]);

    // Add marker to show the footnote to templatecontext.
    $templatecontext['showfootnote'] = true;

    // Add footnote to templatecontext.
    $templatecontext['footnotesetting'] = $footnote;
}
