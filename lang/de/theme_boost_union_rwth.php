<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Union RWTH - Theme config
 *
 * @package    theme_boost_union_rwth
 * @author     2023 Anisa Kusumadewi <kusumadewi@itc-rwth.aachen.de>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();


// General.
$string['pluginname'] = 'Boost Union RWTH';
$string['configtitle'] = 'Boost Union RWTH';
$string['welcome-title:info'] = 'Willkommen bei RWTHmoodle!';
$string['welcome-title:maintenance'] = 'Wartungen';
$string['welcome-url:help-faq:text'] = 'Hilfe & FAQ';
$string['welcome-url:training:text'] = 'Moodle-Schulungen';
$string['welcome-url:sso-login-info:text'] = 'Mehr Informationen zum Login (SSO) ';
$string['welcome-url:local-login-info:text'] = 'Mehr Informationen zum Login (lokale Accounts)';
$string['welcome-url:maintenance-portal:text'] = 'Statusmeldungen';
$string['welcome-url:help-faq:anchor'] = 'https://help.itc.rwth-aachen.de/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/';
$string['welcome-url:training:anchor'] = 'https://www.rwth-aachen.de/cms/root/Die-RWTH/Arbeiten-an-der-RWTH/Personalentwicklung-an-der-RWTH-Aachen/~ebog/Veranstaltungsdatenbank-Personalentwickl/?search=RWTHmoodle&page=&anbieter=ExAcT&showall=1';
$string['welcome-url:sso-login-info:anchor'] = 'https://help.itc.rwth-aachen.de/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/9e974e65be75495294fbe4191119e90b/';
$string['welcome-url:local-login-info:anchor'] = 'https://help.itc.rwth-aachen.de/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/9e974e65be75495294fbe4191119e90b/#2.%20Login%20f%C3%BCr%20lokale%20Accounts';
$string['welcome-url:maintenance-portal:anchor'] = 'https://maintenance.itc.rwth-aachen.de/ticket/status/messages/58-rwthmoodle';
$string['welcome-btn:sso-login'] = 'Login via RWTH Single Sign-on';
$string['welcome-btn:local-login'] = 'Login für lokale Accounts';
$string['welcome-text:info'] = 'RWTHmoodle ist die zentrale, webbasierte Lernplattform der RWTH Aachen.';
$string['welcome-text:sso-login'] = 'Lernräume für Lehrveranstaltungen, Weiterbildungen und zur Prüfungsunterstützung finden Sie nach dem Login mit Ihrem Benutzernamen (Format: ab123456).';
$string['welcome-text:local-login'] = 'Lernräume für studienvorbereitende Veranstaltungen (z. B. Vorkurse) finden Sie nach dem Login in Ihren lokalen Account (Format: veranstaltung_ID).';
$string['welcome-text:maintenance'] = 'Wartungsfenster: Donnerstags zwischen 7:00-8:00 Uhr<br> Das System ist in dieser Zeit ggf. nicht erreichbar.';
$string['welcome-text:maintenance-portal'] = 'Informationen im Störungsfall:';

// Cookie Banner.
$string['cookie-banner-text'] = 'Diese Webseite nutzt technisch notwendige Cookies, um bestmögliche Funktionalität bieten zu können.&nbsp;';
$string['cookie-confirm-button'] = 'Verstanden';
$string['cookie-furtherinfo'] = 'Mehr Info';
$string['cookie-furtherinfo-url'] = '/local/staticpage/view.php?page=dataprivacy#cookies-paragraph';


// Footer
$string['footer-rwth-service'] = 'SERVICE';
$string['footer-rwth-service:arrow:image'] = "$CFG->wwwroot/blocks/rwth_service/pix/link_white.png";
$string['footer-rwth-service:moodle-help'] = 'RWTHmoodle-Hilfe';
$string['footer-rwth-service:moodle-help:anchor'] = 'https://moodle.rwth-aachen.de/help';
$string['footer-rwth-service:media-technology'] = 'Medientechnik C.A.R.L';
$string['footer-rwth-service:media-technology:anchor'] = 'https://hoersaalprojekt.rwth-aachen.de/handreichungen/';
$string['footer-rwth-service:disclaimer'] = 'Datenschutzhinweise';
$string['footer-rwth-service:disclaimer:anchor'] = "$CFG->wwwroot/local/staticpage/view.php?page=dataprivacy";
$string['footer-rwth-service:site-credits'] = 'Impressum';
$string['footer-rwth-service:site-credits:anchor'] = "$CFG->wwwroot/local/staticpage/view.php?page=impressum_de";
$string['footer-contact'] = 'KONTAKT';
$string['footer-contact:mail'] = 'servicedesk@itc.rwth-aachen.de';
$string['footer-contact:mail:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/mail_grey.png";
$string['footer-contact:mail:anchor'] = 'mailto:servicedesk@itc.rwth-aachen.de';
$string['footer-contact:phone'] = '+49 241 / 80-24680';
$string['footer-contact:phone:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/phone_grey.png";
$string['footer-contact:fax'] = '+49 241 / 80-22981';
$string['footer-contact:fax:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/fax_grey.png";
$string['footer-other-rwth-services'] = 'ANDERE RWTH DIENSTE';
$string['footer-other-rwth-services:rwthonline'] = 'RWTHonline';
$string['footer-other-rwth-services:rwthonline:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/logo_rwthonline_grey.png";
$string['footer-other-rwth-services:rwthonline:anchor'] = 'https://online.rwth-aachen.de/';
$string['footer-other-rwth-services:syncmyl2p'] = 'Sync-my-L²P';
$string['footer-other-rwth-services:syncmyl2p:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/logo_sync_my_l2p_grey.png";
$string['footer-other-rwth-services:syncmyl2p:anchor'] = 'https://syncmyl2p.de/';
$string['footer-memberships'] = 'MEMBERSHIPS';
$string['footer-memberships:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/badge_mua_membership.png?ver=3";

/* 
/  Smart menus
*/

// Demo Course rooms
$string['demo-courses-title'] = 'Demolernräume';
$string['smart-menu-demo-courses'] = 'Demolernräume';
$string['smart-menu-demo-courses-block'] = 'Auf dieser Seite finden Sie drei Demolernräume zu RWTHmoodle mit unterschiedlichen inhaltlichen Schwerpunkten. Die Kurse sollen den Lehrenden der RWTH Aachen den Einstieg in die Arbeit mit RWTHmoodle erleichtern.';
$string['smart-menu-functions-overview'] = 'Funktionenübersicht und Tool Guide';
$string['smart-menu-functions-overview-icon'] = 'Icon für Funktionenübersicht und Tool Guide';
$string['smart-menu-functions-overview-block'] = 'Dieser Demolernraum bietet Lehrenden eine Übersicht über die Vielzahl an Lernaktivitäten und weiteren Funktionen, die Sie in einem RWTHmoodle-Lernraum vorfinden und zur digitalen Unterstützung Ihrer Lehre einsetzen können. Sie finden dort viele konkrete Umsetzungsbeispiele und einen kompakten, übersichtlichen Tool Guide, der Ihnen die Auswahl der passenden Werkzeuge erleichtert.';
$string['smart-menu-functions-overview-block-link'] = 'https://moodle.rwth-aachen.de/course/view.php?id=4';
$string['smart-menu-functions-overview-block-link-name'] = 'Zur Selbsteinschreibung in den Lernraum „Funktionenübersicht“';
$string['smart-menu-course-design'] = 'Gute Kursgestaltung';
$string['smart-menu-course-design-icon'] = 'Icon für gute Kursgestaltung';
$string['smart-menu-course-design-block'] = 'Wie strukturiert man einen Lernraum übersichtlich? Wie bindet man einen Semesterplan ein? Wie verleiht man dem Lernen mit To-do-Listen mehr Struktur? Der Demolernraum zur guten Kursgestaltung bietet eine Reihe von Praxistipps für ein schlankes und effizientes Kursdesign, mit dem Sie das Lernen Ihrer Studierenden unterstützen können.';
$string['smart-menu-course-design-block-link'] = 'https://moodle.rwth-aachen.de/course/view.php?id=20478“';
$string['smart-menu-course-design-block-link-name'] = 'Zur Selbsteinschreibung in den Lernraum „Gute Kursgestaltung“';
$string['smart-menu-interactive-mathematical'] = 'Interaktive mathematische Selbsttests mit STACK';
$string['smart-menu-interactive-mathematical-icon'] = 'Icon für interaktive mathematische Selbsttests mit STACK';
$string['smart-menu-interactive-mathematical-block-1'] = 'STACK ist ein Online-Assessment-Paket für Mathematik, mit dem Sie anspruchsvolle (Selbst-)Tests für MINT-Fächer erstellen und Ihren Studierenden individuelles Feedback geben können. Im Demolernraum finden Sie zahlreiche Beispielaufgaben und Selbstlernmaterialien, um sich den Umgang mit STACK anzueignen.
Um Zugang zum ';
$string['smart-menu-interactive-mathematical-block-link-1'] = 'https://moodle.rwth-aachen.de/course/view.php?id=3538';
$string['smart-menu-interactive-mathematical-block-link-name-1'] = 'STACK-Lernraum';
$string['smart-menu-interactive-mathematical-block-2'] = ' zu erhalten, wenden Sie sich bitte an das ';
$string['smart-menu-interactive-mathematical-block-link-2'] = 'mailto:servicedesk@itc.rwth-aachen.de';
$string['smart-menu-interactive-mathematical-block-link-name-2'] = 'IT-ServiceDesk';

//studying and teaching
$string['studying-and-teaching-title'] = 'Weitere Angebote zu Studium und Lehre';
$string['smart-menu-studying-and-teaching'] = 'Weitere Angebote zu Studium und Lehre';
$string['smart-menu-studying-and-teaching-block'] = 'Auf dieser Seite finden Sie einige gesondert aufbereitete Zusatzinformationen zu RWTHmoodle sowie weiterführende Angebote rund um Studium und Lehre an der RWTH Aachen.';
$string['smart-menu-accessibility'] = 'Barrierefreiheit in RWTHmoodle';
$string['smart-menu-accessibility-icon'] = 'Icon für Barrierefreiheit in RWTHmoodle';
$string['smart-menu-accessibility-block-1'] = 'Das Lernmanagement-System Moodle ist in der Version 4.2 als barrierefrei nach WCAG 2.1, Level AA zertifiziert. Details entnehmen Sie bitte der Erklärung zur Barrierefreiheit im Footer der Seite. In unserer Dokumentation finden Sie zusätzliche Informationen und Hilfestellungen zur ';
$string['smart-menu-accessibility-block-link-1'] = 'https://help.itc.rwth-aachen.de/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/37d227dcfbda421d8d1d8fd5e32a2e84/';
$string['smart-menu-accessibility-block-link-name-1'] = 'Barrierefreiheit in RWTHmoodle';
$string['smart-menu-accessibility-block-2'] = ', unter anderem Selbstlernkurse und Werkzeuge, mit denen Sie als Lehrende Ihren Lernraum auf Barrierefreiheit prüfen können.';
$string['smart-menu-copyright'] = 'Urheberrecht in RWTHmoodle';
$string['smart-menu-copyright-icon'] = 'Icon für Urheberrecht in RWTHmoodle';
$string['smart-menu-copyright-block-1'] = 'Für die Verwendung urheberrechtlich geschützter Materialien in RWTHmoodle-Lernräumen gelten - für Lehrende wie Studierende - einige grundlegende Regeln. Unsere ';
$string['smart-menu-copyright-block-link-1'] = 'https://help.itc.rwth-aachen.de/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/10efbba22a8a4ed798c8b8ff377aee65/';
$string['smart-menu-copyright-block-link-name-1'] = 'Merkblätter zum Urheberrecht';
$string['smart-menu-copyright-block-2'] = ' klären auf, was erlaubt ist und was nicht.';
$string['smart-menu-anonymity'] = 'Anonymität in RWTHmoodle, Netiquette und allgemeine Verhaltensregeln';
$string['smart-menu-anonymity-icon'] = 'Icon für Anonymität in RWTHmoodle, Netiquette und allgemeine Verhaltensregeln';
$string['smart-menu-anonymity-block-1'] = 'Einige Aktivitäten in RWTHmoodle erlauben anonyme Beiträge, so zum Beispiel das Anonyme Forum oder die PDF-Annotation. "Anonymität" in RWTHmoodle ist allerdings nicht gleichbedeutend mit vollständiger Anonymität. Beachten Sie daher die ';
$string['smart-menu-anonymity-block-link-1'] = 'https://help.itc.rwth-aachen.de/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/5554dc82b63244af8882163f3760b761/';
$string['smart-menu-anonymity-block-link-name-1'] = 'Hinweise zu Anonymität in RWTHmoodle';
$string['smart-menu-anonymity-block-2'] = '. Beachten Sie zudem unsere ';
$string['smart-menu-anonymity-block-link-2'] = 'https://help.itc.rwth-aachen.de/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/bae1444c7ea74c70a97e252c8ea94429/';
$string['smart-menu-anonymity-block-link-name-2'] = 'Netiquette und allgemeinen Verhaltensregeln';
$string['smart-menu-handouts'] = 'Handreichungen Hörsaalausstattung';
$string['smart-menu-handouts-icon'] = 'Icon für Anonymität in RWTHmoodle, Netiquette und allgemeine Verhaltensregeln';
$string['smart-menu-handouts-block-1'] = 'In den Hörsälen H01-H03 im C.A.R.L. und im Hörsaal Physik können Lehrende die vorhandene Medientechnik für automatische Veranstaltungsaufzeichnungen nutzen, die im passenden RWTHmoodle-Lernraum abgelegt werden. In den ';
$string['smart-menu-handouts-block-link-1'] = 'https://moodle.rwth-aachen.de/course/view.php?id=29645';
$string['smart-menu-handouts-block-link-name-1'] = 'Handreichungen Hörsaalausstattung';
$string['smart-menu-handouts-block-2'] = ' finden Sie alle nötigen Informationen für die Nutzung.';
$string['smart-menu-lehrbar'] = 'LehrBar';
$string['smart-menu-lehrbar-icon'] = 'Icon für LehrBar';
$string['smart-menu-lehrbar-block-1'] = 'Die ';
$string['smart-menu-lehrbar-block-link-1'] = 'https://moodle.rwth-aachen.de/course/view.php?id=12891';
$string['smart-menu-lehrbar-block-link-name-1'] = 'LehrBar';
$string['smart-menu-lehrbar-block-2'] = ' des CLS bietet allen Lehrenden der RWTH Aachen vielfältige Tipps, Tricks und Inspiration rund um das Thema Lehren an der Hochschule.';
$string['smart-menu-studierbar'] = 'StudierBar';
$string['smart-menu-studierbar-icon'] = 'Icon für StudierBar';
$string['smart-menu-studierbar-block-1'] = 'Die ';
$string['smart-menu-studierbar-block-link-1'] = 'https://moodle.rwth-aachen.de/course/view.php?id=30920';
$string['smart-menu-studierbar-block-link-name-1'] = 'StudierBar';
$string['smart-menu-studierbar-block-2'] = ' richtet sich an die Studierenden der RWTH Aachen. Hier findet Ihr zahlreiche Informationen und Links zu den Themen Hochschul-, Prüfungs- und Studienorganisation sowie die Adressen wichtiger Beratungsstellen, die Unterstützung bieten bei Problemen im Studium.';


