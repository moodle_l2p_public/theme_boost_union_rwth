<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Union RWTH - Theme config
 *
 * @package    theme_boost_union_rwth
 * @author     2023 Anisa Kusumadewi <kusumadewi@itc-rwth.aachen.de>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// General.
$string['pluginname'] = 'Boost Union RWTH';
$string['configtitle'] = 'Boost Union RWTH';
$string['choosereadme'] = 'Theme Boost Union RWTH is an enhanced child theme of Boost Union provided by Moodle an Hochschulen e.V.';
$string['welcome-title:info'] = 'Welcome to RWTHmoodle!';
$string['welcome-title:maintenance'] = 'Maintenance';
$string['welcome-url:help-faq:text'] = 'Help & FAQ';
$string['welcome-url:training:text'] = 'Trainings';
$string['welcome-url:sso-login-info:text'] = 'More information on login (SSO)';
$string['welcome-url:local-login-info:text'] = 'More information on login (local accounts)';
$string['welcome-url:maintenance-portal:text'] = 'Status updates';
$string['welcome-url:help-faq:anchor'] = 'https://help.itc.rwth-aachen.de/en/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/';
$string['welcome-url:training:anchor'] = 'https://www.rwth-aachen.de/cms/root/Die-RWTH/Karriere/Personalentwicklung-an-der-RWTH-Aachen/~ebog/Veranstaltungsdatenbank-Personalentwickl/?search=RWTHmoodle&anbieter=ExAcT&showall=1&lidx=1';
$string['welcome-url:sso-login-info:anchor'] = 'https://help.itc.rwth-aachen.de/en/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/9e974e65be75495294fbe4191119e90b/';
$string['welcome-url:local-login-info:anchor'] = 'https://help.itc.rwth-aachen.de/en/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/9e974e65be75495294fbe4191119e90b/#2.%20Login%20f%C3%BCr%20lokale%20Accounts';
$string['welcome-url:maintenance-portal:anchor'] = 'https://maintenance.itc.rwth-aachen.de/ticket/status/messages/58-rwthmoodle';
$string['welcome-btn:sso-login'] = 'Login via RWTH Single Sign-on';
$string['welcome-btn:local-login'] = 'Login for local accounts';
$string['welcome-text:info'] = 'RWTHmoodle is the central web-based learning platform of the RWTH Aachen University.';
$string['welcome-text:sso-login'] = 'Login using your user name (ab123456) to access your course rooms for lectures, professional training, and exam support.';
$string['welcome-text:local-login'] = 'Login using your local account (event_ID) to access course rooms for study-preparatory events (e.g., bridge courses).';
$string['welcome-text:maintenance'] = 'Weekly maintenance: Thursdays, 7.00 – 8.00 am<br> RWTHmoodle is not available during this time.';
$string['welcome-text:maintenance-portal'] = 'Info on malfunctions:';

// Cookie Banner.
$string['cookie-banner-text'] = 'This website uses technically necessary cookies to offer the best possible functionality.&nbsp;';
$string['cookie-confirm-button'] = 'Accept';
$string['cookie-furtherinfo'] = 'More Information';
$string['cookie-furtherinfo-url'] = '/local/staticpage/view.php?page=dataprivacy_en#cookies-paragraph';

// Footer
$string['footer-rwth-service'] = 'SERVICE';
$string['footer-rwth-service:arrow:image'] = "$CFG->wwwroot/blocks/rwth_service/pix/link_white.png";
$string['footer-rwth-service:moodle-help'] = 'RWTHmoodle help';
$string['footer-rwth-service:moodle-help:anchor'] = 'https://moodle.rwth-aachen.de/help';
$string['footer-rwth-service:media-technology'] = 'Media Technology C.A.R.L';
$string['footer-rwth-service:media-technology:anchor'] = 'https://hoersaalprojekt.rwth-aachen.de/handreichungen/';
$string['footer-rwth-service:disclaimer'] = 'Disclaimer';
$string['footer-rwth-service:disclaimer:anchor'] = "$CFG->wwwroot/local/staticpage/view.php?page=dataprivacy_en";
$string['footer-rwth-service:site-credits'] = 'Site Credits';
$string['footer-rwth-service:site-credits:anchor'] = "$CFG->wwwroot/local/staticpage/view.php?page=impressum_en";
$string['footer-contact'] = 'CONTACT';
$string['footer-contact:mail'] = 'servicedesk@itc.rwth-aachen.de';
$string['footer-contact:mail:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/mail_grey.png";
$string['footer-contact:mail:anchor'] = 'mailto:servicedesk@itc.rwth-aachen.de';
$string['footer-contact:phone'] = '+49 241 / 80-24680';
$string['footer-contact:phone:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/phone_grey.png";
$string['footer-contact:fax'] = '+49 241 / 80-22981';
$string['footer-contact:fax:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/fax_grey.png";
$string['footer-other-rwth-services'] = 'OTHER RWTH SERVICES';
$string['footer-other-rwth-services:rwthonline'] = 'RWTHonline';
$string['footer-other-rwth-services:rwthonline:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/logo_rwthonline_grey.png";
$string['footer-other-rwth-services:rwthonline:anchor'] = 'https://online.rwth-aachen.de/';
$string['footer-other-rwth-services:syncmyl2p'] = 'Sync-my-L²P';
$string['footer-other-rwth-services:syncmyl2p:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/logo_sync_my_l2p_grey.png";
$string['footer-other-rwth-services:syncmyl2p:anchor'] = 'https://syncmyl2p.de/';
$string['footer-memberships'] = 'MEMBERSHIPS';
$string['footer-memberships:image'] = "$CFG->wwwroot/admin/tool/rwth_blocks/pix/badge_mua_membership.png?ver=3";


/* 
/  Smart menus
*/

// Demo Course rooms
$string['demo-courses-title'] = 'Demo course rooms';
$string['smart-menu-demo-courses'] = 'Demo course rooms';
$string['smart-menu-demo-courses-block'] = 'On this page you will find three demon course rooms on RWTHmoodle with different focal points. The courses are designed to make it easier for RWTH Aachen University teaching staff to get started with RWTHmoodle.';
$string['smart-menu-functions-overview'] = 'Functions Overview and Tool Guide';
$string['smart-menu-functions-overview-icon'] = 'Icon for Functions Overview and Tool Guide';
$string['smart-menu-functions-overview-block'] = 'This demo course room provides teachers with an overview of the wide range of learning activities and other functions that you can find in an RWTHmoodle course room and that you may use to digitally support your teaching. You will find many concrete examples of implementation and a compact, clear tool guide to help you choose the right tools.';
$string['smart-menu-functions-overview-block-link'] = 'https://moodle.rwth-aachen.de/course/view.php?id=24736';
$string['smart-menu-functions-overview-block-link-name'] = 'Go to self-enrolment for the course room „Functions Overview“';
$string['smart-menu-course-design'] = 'Good Course Design';
$string['smart-menu-course-design-icon'] = 'Icon for Good Course Design';
$string['smart-menu-course-design-block'] = "How do you structure a course room? How do you integrate a semester plan? How do you give learning more structure with to-do lists? The demo course on Good Course Design offers a range of practical tips for a lean and efficient course design that you can use to support your students' learning.";
$string['smart-menu-course-design-block-link'] = 'https://moodle.rwth-aachen.de/course/view.php?id=24737';
$string['smart-menu-course-design-block-link-name'] = 'Go to self-enrolment for the course room „Good Course Design“';
$string['smart-menu-interactive-mathematical'] = 'Interactive mathematical quizzes with STACK';
$string['smart-menu-interactive-mathematical-icon'] = 'Icon for Interactive mathematical quizzes with STACK';
$string['smart-menu-interactive-mathematical-block-1'] = 'STACK is an online assessment package for mathematics that you can use to create challenging (self-)tests for STEM subjects and give your students individual feedback. In the demo course room you will find numerous sample questions and self-study materials to help you learn how to use STACK. The course content is mainly in German language.
To get access to the ';
$string['smart-menu-interactive-mathematical-block-link-1'] = 'https://moodle.rwth-aachen.de/course/view.php?id=3538';
$string['smart-menu-interactive-mathematical-block-link-name-1'] = 'STACK course room';
$string['smart-menu-interactive-mathematical-block-2'] = ' please contact the ';
$string['smart-menu-interactive-mathematical-block-link-2'] = 'mailto:servicedesk@itc.rwth-aachen.de';
$string['smart-menu-interactive-mathematical-block-link-name-2'] = 'IT-ServiceDesk';

//studying and teaching
$string['studying-and-teaching-title'] = 'Further offers on teaching and studying';
$string['smart-menu-studying-and-teaching'] = 'Further offers on teaching and studying';
$string['smart-menu-studying-and-teaching-block'] = 'On this page you will find additional information on RWTHmoodle as well as further offers concerning studying and teaching at RWTH Aachen University.';
$string['smart-menu-accessibility'] = 'Accessibility in RWTHmoodle';
$string['smart-menu-accessibility-icon'] = 'Icon for Accessibility in RWTHmoodle';
$string['smart-menu-accessibility-block-1'] = 'Version 4.2 of the Learning Management System Moodle is certified as accessible according to WCAG 2.1, Level AA. For details, please refer to the accessibility statement in the footer of the page. In our documentation you will find additional information and assistance on ';
$string['smart-menu-accessibility-block-link-1'] = 'https://help.itc.rwth-aachen.de/en/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/37d227dcfbda421d8d1d8fd5e32a2e84/';
$string['smart-menu-accessibility-block-link-name-1'] = 'accessibility in RWTHmoodle';
$string['smart-menu-accessibility-block-2'] = ', including self-study courses and tools that you can use as a teacher to check your course room for accessibility.';
$string['smart-menu-copyright'] = 'Copyright in RWTHmoodle';
$string['smart-menu-copyright-icon'] = 'Icon for Copyright in RWTHmoodle';
$string['smart-menu-copyright-block-1'] = 'Some basic rules apply to the use of copyrighted materials in RWTHmoodle course rooms - for teachers and students alike. Our ';
$string['smart-menu-copyright-block-link-1'] = 'https://help.itc.rwth-aachen.de/en/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/10efbba22a8a4ed798c8b8ff377aee65/';
$string['smart-menu-copyright-block-link-name-1'] = 'copyright information sheets';
$string['smart-menu-copyright-block-2'] = ' explain what is allowed and what is not.';
$string['smart-menu-anonymity'] = 'Anonymity in RWTHmoodle, netiquette and general rules of conduct';
$string['smart-menu-anonymity-icon'] = 'Icon for Anonymity in RWTHmoodle, netiquette and general rules of conduct';
$string['smart-menu-anonymity-block-1'] = 'Some activities in RWTHmoodle allow anonymous contributions, such as the Anonymous Forum or PDF annotation. However, “anonymity” in RWTHmoodle is not synonymous with complete anonymity. Therefore, please note the information on ';
$string['smart-menu-anonymity-block-link-1'] = 'https://help.itc.rwth-aachen.de/en/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/5554dc82b63244af8882163f3760b761/';
$string['smart-menu-anonymity-block-link-name-1'] = 'anonymity in RWTHmoodle';
$string['smart-menu-anonymity-block-2'] = '. Please also observe our ';
$string['smart-menu-anonymity-block-link-2'] = 'https://help.itc.rwth-aachen.de/en/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/bae1444c7ea74c70a97e252c8ea94429/';
$string['smart-menu-anonymity-block-link-name-2'] = 'netiquette and general rules of conduct';
$string['smart-menu-handouts'] = 'Lecture hall equipment handouts';
$string['smart-menu-handouts-icon'] = 'Icon for Lecture hall equipment handouts';
$string['smart-menu-handouts-block-1'] = 'In the lecture halls H01-H03 in the C.A.R.L. and in the physics lecture hall, lecturers can use the existing media technology for automatic lecture recordings, which are stored in the appropriate RWTHmoodle course room. You will find all the information you need to use the equipment in the ';
$string['smart-menu-handouts-block-link-1'] = 'https://moodle.rwth-aachen.de/course/view.php?id=29645';
$string['smart-menu-handouts-block-link-name-1'] = 'handouts for the lecture halls';
$string['smart-menu-handouts-block-2'] = ' (German only).';
$string['smart-menu-lehrbar'] = 'LehrBar';
$string['smart-menu-lehrbar-icon'] = 'Icon für LehrBar';
$string['smart-menu-lehrbar-block-1'] = 'The ';
$string['smart-menu-lehrbar-block-link-1'] = 'https://moodle.rwth-aachen.de/course/view.php?id=12891';
$string['smart-menu-lehrbar-block-link-name-1'] = 'LehrBar';
$string['smart-menu-lehrbar-block-2'] = ' by the CLS offers all teaching staff at RWTH Aachen University a wide range of tips, tricks and inspiration on all aspects of teaching at the university. The information is in German only.';
$string['smart-menu-studierbar'] = 'StudierBar';
$string['smart-menu-studierbar-icon'] = 'Icon for StudierBar';
$string['smart-menu-studierbar-block-1'] = 'The ';
$string['smart-menu-studierbar-block-link-1'] = 'https://moodle.rwth-aachen.de/course/view.php?id=30920';
$string['smart-menu-studierbar-block-link-name-1'] = 'StudierBar';
$string['smart-menu-studierbar-block-2'] = ' is aimed at students at RWTH. Here you will find a wealth of information and links on the topics of university, examination and study organization as well as the addresses of important advice centres that offer support with problems during your studies.';

