<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   theme_boost_union_rwth
 * @copyright 2024 RWTH Aachen University
 * @author    m.kirch@itc.rwth-aachen.de
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');

$context = context_system::instance();

// Set page URL.
$PAGE->set_url("$CFG->wwwroot/theme/boost_union_rwth/demo_courses.php");

// Set page layout.
$PAGE->set_pagelayout(pagelayout: 'standard');

// Set page context.
$PAGE->set_context($context);

// Set page title.
$PAGE->set_title(get_string('demo-courses-title', 'theme_boost_union_rwth'));

// Start page output.
echo $OUTPUT->header();

$templatecontext = [];
echo $OUTPUT->render_from_template('theme_boost_union_rwth/news_and_help/demo_courses', $templatecontext);

// Finish page.
echo $OUTPUT->footer();